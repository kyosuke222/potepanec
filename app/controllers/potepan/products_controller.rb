# frozen_string_literal: true

class Potepan::ProductsController < ApplicationController
  MAX_RELATED_PRODUCT_COUNT = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: %i[default_price images]).limit(MAX_RELATED_PRODUCT_COUNT)
  end
end
