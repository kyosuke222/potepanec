# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Spree::ProductDecorator, type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy_id: taxonomy) }
  let!(:product) { create(:product, taxon_ids: taxon.id) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    other_taxon = create(:taxon, taxonomy_id: taxonomy)
    create(:product, taxon_ids: other_taxon.id)
  end

  describe '関連商品のテスト' do
    it '関連商品が含まれること' do
      expect(product.related_products).to match_array related_products
    end

    it 'メイン商品は関連商品に表示されない' do
      expect(product.related_products).not_to include product
    end

    it '関連商品に商品の重複がない' do
      expect(product.related_products).to eq product.related_products.uniq
    end
  end
end
