# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'ページタイトルのテスト' do
    context '@product.nameに値が入っている場合' do
      it '@product.nameとBASE_TITLEが表示される' do
        expect(full_title('RUBY ON RAILS TOTE')).to eq('RUBY ON RAILS TOTE - BIGBAG Store')
      end
    end

    context '@product.nameが空白の場合' do
      it 'BASE_TITLEだけ表示される' do
        expect(full_title('')).to eq('BIGBAG Store')
      end
    end

    context '@product.nameがnilの場合' do
      it 'BASE_TITLEだけ表示される' do
        expect(full_title(nil)).to eq('BIGBAG Store')
      end
    end
  end
end
