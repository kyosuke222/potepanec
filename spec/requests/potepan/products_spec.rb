# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let(:unrelated_taxon) { create(:taxon) }
  let(:unrelated_product) { create(:product, taxons: [unrelated_taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  describe 'カテゴリーページの表示テスト' do
    it '商品詳細ページの表示に成功する' do
      expect(response).to have_http_status 200
    end

    it 'productに期待する値が入っている' do
      expect(assigns(:product)).to eq product
    end

    it '商品詳細画面に商品名が含まれる' do
      expect(response.body).to include product.name
    end

    it '商品詳細画面に商品価格が含まれる' do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品詳細画面に商品説明が含まれる' do
      expect(response.body).to include product.description
    end

    it '商品詳細画面に関連商品画像が含まれる' do
      within '.productBox' do
        expect(response.body).to have_content related_product.display_image.attachment(:product)
      end
    end

    it '商品詳細画面に関連商品が4つ表示される' do
      expect(assigns(:related_products).size).to eq Potepan::ProductsController::MAX_RELATED_PRODUCT_COUNT
    end

    it '関連しない商品名は表示されない' do
      expect(response.body).not_to include unrelated_product.name
    end
  end
end
