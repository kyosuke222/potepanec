# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Potepan::Categories', type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy_id: taxonomy) }
  let!(:product) { create(:product, taxon_ids: taxon.id) }
  let(:other_taxon) { create(:taxon, taxonomy_id: taxonomy) }
  let!(:other_product) { create(:product, taxon_ids: other_taxon.id) }

  before do
    get potepan_category_path(taxon.id)
  end

  describe 'カテゴリーページの表示テスト' do
    it 'カテゴリーページの表示に成功する' do
      expect(response).to have_http_status 200
    end

    it 'カテゴリーページに商品カテゴリーが表示される' do
      expect(response.body).to include taxonomy.name
    end

    it 'カテゴリーページにカテゴリー名が表示される' do
      expect(response.body).to include taxon.name
    end

    it 'カテゴリーページに商品名が表示される' do
      expect(response.body).to include product.name
    end

    it 'カテゴリーページに他の商品は表示されない' do
      expect(response.body).not_to include other_product.name
    end
  end
end
