# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon) }
  let(:image) { create(:image) }
  let(:variant) { create(:variant, images: [image]) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon], master: variant) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    driven_by(:rack_test)
    visit potepan_product_path(product.id)
  end

  describe '商品詳細ページの動作テスト' do
    it '一覧ページへをクリックするとカテゴリー一覧へ戻る' do
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it '関連商品名が正しく表示される' do
      expect(page).to have_content related_product.name
    end

    it '関連商品価格が正しく表示される' do
      expect(page).to have_content related_product.display_price
    end

    it '関連商品画像が正しく表示される' do
      expect(page).to have_selector "img[src$='#{related_product.display_image.attachment(:product)}']"
    end

    it '関連商品が4つ表示される' do
      expect(page).to have_selector '.productBox', Potepan::ProductsController::MAX_RELATED_PRODUCT_COUNT
    end

    it '関連商品名をクリックすると商品詳細ページに遷移する' do
      click_on related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end
end
