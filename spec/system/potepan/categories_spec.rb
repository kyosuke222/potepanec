# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Potepan::Categories', type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    driven_by(:rack_test)
    visit potepan_category_path(product.taxons.first.id)
  end

  describe 'カテゴリーページの動作テスト' do
    it '商品カテゴリーが表示される' do
      within '.side-nav' do
        expect(page).to have_content taxonomy.name
      end
    end

    it '商品カテゴリーをクリックするとカテゴリー名が表示される' do
      click_on taxonomy.name
      expect(page).to have_content taxon.name
    end

    it 'カテゴリー名をクリックするとカテゴリー別商品一覧ページが表示される' do
      click_on taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it '商品名をクリックすると商品詳細ページが表示される' do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it 'サイドバーのカテゴリの個数と表示される商品の数が正しい' do
      expect(page).to have_css '.productBox', count: taxon.products.count
    end

    it '商品名が表示される' do
      within '.productBox' do
        expect(page).to have_content product.name
      end
    end

    it '商品価格が表示される' do
      within '.productBox' do
        expect(page).to have_content product.price
      end
    end
  end
end
